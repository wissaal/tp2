package com.example.uapv1702827.tp2bis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BookActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        final EditText ti = findViewById(R.id.nameBook);
        final EditText au = findViewById(R.id.editAuthors);
        final EditText ye = findViewById(R.id.editYear);
        final EditText ge = findViewById(R.id.editGenres);
        final EditText pu = findViewById(R.id.editPublisher);

        final Book book = getIntent().getExtras().getParcelable("book");

        if (book != null) {
            ti.setText(book.getTitle(), TextView.BufferType.EDITABLE);
            au.setText(book.getAuthors(), TextView.BufferType.EDITABLE);
            ye.setText(book.getYear(), TextView.BufferType.EDITABLE);
            ge.setText(book.getGenres(), TextView.BufferType.EDITABLE);
            pu.setText(book.getPublisher(), TextView.BufferType.EDITABLE);

        }

        Button save = (Button) findViewById(R.id.button);

        assert save != null;
        save.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                BookDbHelper bdh= new BookDbHelper(BookActivity.this);

                if (book != null) {
                    book.setTitle(ti.getText().toString());
                    book.setAuthors(au.getText().toString());
                    book.setYear(ye.getText().toString());
                    book.setGenres(ge.getText().toString());
                    book.setPublisher(pu.getText().toString());
                    bdh.updateBook(book);

                } else {
                    Book book_= new Book();

                    book_.setTitle(ti.getText().toString());
                    book_.setAuthors(au.getText().toString());
                    book_.setYear(ye.getText().toString());
                    book_.setGenres(ge.getText().toString());
                    book_.setPublisher(pu.getText().toString());
                    bdh.addBook(book_);
                }



                Intent intent = new Intent(BookActivity.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }
}
