package com.example.uapv1702827.tp2bis;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.SimpleCursorAdapter;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.Snackbar;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    BookDbHelper data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton bouton=findViewById(R.id.bouton);
        final Intent in=new Intent(this,BookActivity.class);
        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        data = new BookDbHelper(getApplicationContext());
        //data.populate();

        final Cursor curs=data.fetchAllBooks();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, curs, new String[]{BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS}, new int[]{android.R.id.text1, android.R.id.text2}, 0);

        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor =(Cursor)listView.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                Book book= data.cursorToBook(cursor);
                intent.putExtra("book", book);
                startActivity(intent);
            }
        });

        bouton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", (Book)null);
                startActivity(intent);
            }
        });

        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                AdapterView.AdapterContextMenuInfo mi = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.add(0, 0, 0, "Delete");
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView adapter = (AdapterView) findViewById(R.id.list);
        Cursor cursor = (Cursor) adapter.getItemAtPosition(info.position);
        BookDbHelper dbh = new BookDbHelper(MainActivity.this);
        dbh.deleteBook(cursor);
        Intent intent =new Intent(MainActivity.this,MainActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
